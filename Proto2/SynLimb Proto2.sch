EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Bertolt Meyer SynLimb Interface"
Date "2020-06-21"
Rev "Proto2"
Comp "Christian Zollner"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1350 1300 2825 1875
U 5DAA151B
F0 "Power / Mechanic" 60
F1 "PowerMechanic.sch" 60
$EndSheet
$Sheet
S 6400 1300 2825 1875
U 5DAA23CB
F0 "Analog Interface" 60
F1 "Analog Interface.sch" 60
$EndSheet
Text Notes 3675 1000 0    157  ~ 31
SynLimb Synth Prosthetics
Text Notes 6400 4050 0    118  ~ 0
To Do:\n- umschalter für invertierte outputs\n- gate outs
Text Notes 1625 2325 0    157  ~ 31
Power / Mechanic
Text Notes 6775 2325 0    157  ~ 31
Analog Interfaces
Text Notes 1325 4200 0    118  ~ 0
Idears:\n- Switch between several channels (touch plates)\n- Gyros\n- Triggers / Gates
$Sheet
S 1350 4650 2850 1800
U 5ECD61F5
F0 "LED Bars" 50
F1 "LED Bars.sch" 50
$EndSheet
Text Notes 1750 5700 0    157  ~ 31
LED Bar Graphs
$Sheet
S 6400 4650 2850 1800
U 5FD22934
F0 "Connections" 50
F1 "Connections.sch" 50
$EndSheet
Text Notes 7150 5700 0    157  ~ 31
Connectors
$EndSCHEMATC
