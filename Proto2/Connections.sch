EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5FD2BD2E
P 3800 2050
AR Path="/5ECD61F5/5FD2BD2E" Ref="J?"  Part="1" 
AR Path="/5FD22934/5FD2BD2E" Ref="J4"  Part="1" 
F 0 "J4" H 3900 2300 50  0000 C CNN
F 1 "LED-Conn-MA" H 4150 2450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3800 2050 50  0001 C CNN
F 3 "~" H 3800 2050 50  0001 C CNN
	1    3800 2050
	1    0    0    -1  
$EndComp
Text GLabel 4000 2050 2    39   Input ~ 0
CV-A-LED-Sig
$Comp
L Connector:Conn_01x03_Male J?
U 1 1 5FD34287
P 3800 3050
AR Path="/5ECD61F5/5FD34287" Ref="J?"  Part="1" 
AR Path="/5FD22934/5FD34287" Ref="J6"  Part="1" 
F 0 "J6" H 3900 3300 50  0000 C CNN
F 1 "LED-Conn-MB" H 4150 3450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3800 3050 50  0001 C CNN
F 3 "~" H 3800 3050 50  0001 C CNN
	1    3800 3050
	1    0    0    -1  
$EndComp
Text GLabel 4000 3050 2    39   Input ~ 0
CV-B-LED-Sig
$Comp
L Connector:Conn_01x03_Female J5
U 1 1 5FD34F04
P 3800 2050
F 0 "J5" H 3700 2300 50  0000 C CNN
F 1 "LED-Conn-FA" H 3500 2450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3800 2050 50  0001 C CNN
F 3 "~" H 3800 2050 50  0001 C CNN
	1    3800 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J7
U 1 1 5FD36BAB
P 3800 3050
F 0 "J7" H 3700 3300 50  0000 C CNN
F 1 "LED-Conn-FB" H 3500 3450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3800 3050 50  0001 C CNN
F 3 "~" H 3800 3050 50  0001 C CNN
	1    3800 3050
	1    0    0    -1  
$EndComp
Text GLabel 3600 3050 0    39   Input ~ 0
CV-B
$Comp
L power:VCC #PWR?
U 1 1 5FD377A4
P 3100 2950
AR Path="/5ECD61F5/5FD377A4" Ref="#PWR?"  Part="1" 
AR Path="/5FD22934/5FD377A4" Ref="#PWR0136"  Part="1" 
F 0 "#PWR0136" H 3100 2800 50  0001 C CNN
F 1 "VCC" H 3115 3123 50  0000 C CNN
F 2 "" H 3100 2950 50  0001 C CNN
F 3 "" H 3100 2950 50  0001 C CNN
	1    3100 2950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 2950 3600 2950
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR?
U 1 1 5FD377AF
P 3100 3200
AR Path="/5ECD61F5/5FD377AF" Ref="#PWR?"  Part="1" 
AR Path="/5FD22934/5FD377AF" Ref="#PWR0137"  Part="1" 
F 0 "#PWR0137" H 3100 3200 40  0001 C CNN
F 1 "AGND" H 3100 3100 50  0000 C CNN
F 2 "" H 3100 3200 60  0000 C CNN
F 3 "" H 3100 3200 60  0000 C CNN
	1    3100 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 3200 3100 3150
Wire Wire Line
	3100 3150 3600 3150
Text GLabel 3600 2050 0    39   Input ~ 0
CV-A
$Comp
L power:VCC #PWR?
U 1 1 5FD3BA7D
P 3100 1950
AR Path="/5ECD61F5/5FD3BA7D" Ref="#PWR?"  Part="1" 
AR Path="/5FD22934/5FD3BA7D" Ref="#PWR0138"  Part="1" 
F 0 "#PWR0138" H 3100 1800 50  0001 C CNN
F 1 "VCC" H 3115 2123 50  0000 C CNN
F 2 "" H 3100 1950 50  0001 C CNN
F 3 "" H 3100 1950 50  0001 C CNN
	1    3100 1950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 1950 3600 1950
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR?
U 1 1 5FD3BA88
P 3100 2200
AR Path="/5ECD61F5/5FD3BA88" Ref="#PWR?"  Part="1" 
AR Path="/5FD22934/5FD3BA88" Ref="#PWR0139"  Part="1" 
F 0 "#PWR0139" H 3100 2200 40  0001 C CNN
F 1 "AGND" H 3100 2100 50  0000 C CNN
F 2 "" H 3100 2200 60  0000 C CNN
F 3 "" H 3100 2200 60  0000 C CNN
	1    3100 2200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 2200 3100 2150
Wire Wire Line
	3100 2150 3600 2150
Text Notes 3350 1400 0    118  ~ 24
LED PCBs
$Comp
L Connector:Conn_01x04_Female J8
U 1 1 5FD5EF3D
P 6500 2050
F 0 "J8" H 6400 2300 50  0000 C CNN
F 1 "Sink-2.4-Conn-Sig" H 6150 2450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6500 2050 50  0001 C CNN
F 3 "~" H 6500 2050 50  0001 C CNN
	1    6500 2050
	1    0    0    -1  
$EndComp
Text GLabel 6300 2050 0    39   Input ~ 0
CV-A
$Comp
L power:VCC #PWR?
U 1 1 5FD5EF48
P 5800 1950
AR Path="/5ECD61F5/5FD5EF48" Ref="#PWR?"  Part="1" 
AR Path="/5FD22934/5FD5EF48" Ref="#PWR0140"  Part="1" 
F 0 "#PWR0140" H 5800 1800 50  0001 C CNN
F 1 "VCC" H 5815 2123 50  0000 C CNN
F 2 "" H 5800 1950 50  0001 C CNN
F 3 "" H 5800 1950 50  0001 C CNN
	1    5800 1950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5800 1950 6300 1950
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR?
U 1 1 5FD5EF53
P 5800 2300
AR Path="/5ECD61F5/5FD5EF53" Ref="#PWR?"  Part="1" 
AR Path="/5FD22934/5FD5EF53" Ref="#PWR0141"  Part="1" 
F 0 "#PWR0141" H 5800 2300 40  0001 C CNN
F 1 "AGND" H 5800 2200 50  0000 C CNN
F 2 "" H 5800 2300 60  0000 C CNN
F 3 "" H 5800 2300 60  0000 C CNN
	1    5800 2300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5800 2300 5800 2250
Wire Wire Line
	5800 2250 6300 2250
Text GLabel 6300 2150 0    39   Input ~ 0
CV-B
$Comp
L Connector:Conn_01x04_Female J9
U 1 1 5FD689E4
P 7500 2050
F 0 "J9" H 7400 2300 50  0000 C CNN
F 1 "Sink-2.4-Conn-Mech" H 7350 2450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7500 2050 50  0001 C CNN
F 3 "~" H 7500 2050 50  0001 C CNN
	1    7500 2050
	1    0    0    -1  
$EndComp
NoConn ~ 7300 1950
NoConn ~ 7300 2050
Text Notes 6250 1400 0    118  ~ 24
Sink 2.4
Text GLabel 4000 2950 2    39   Input ~ 0
VCC-LEDs-B
Text GLabel 4000 3150 2    39   Input ~ 0
GND-LEDs-B
Text GLabel 4000 1950 2    39   Input ~ 0
VCC-LEDs-A
Text GLabel 4000 2150 2    39   Input ~ 0
GND-LEDs-A
Text GLabel 7300 2250 0    39   Input ~ 0
+12V
Text GLabel 7300 2150 0    39   Input ~ 0
-12V
$EndSCHEMATC
