EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Bertold Meyer Analog Interface"
Date "2019-10-15"
Rev "0"
Comp "KOMA Elektronik"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4276 4815 0    118  ~ 24
ANALOG SENSOR INTERFACE B
Wire Notes Line
	11100 3980 585  3980
Text Notes 4275 975  0    118  ~ 24
ANALOG SENSOR INTERFACE A
$Comp
L KomaFieldKit-PR3-rescue:TLV274 U2
U 2 1 5DACBD6F
P 3145 1835
F 0 "U2" H 3195 2035 60  0000 C CNN
F 1 "TL074" H 3295 1635 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3145 1835 60  0001 C CNN
F 3 "" H 3145 1835 60  0000 C CNN
	2    3145 1835
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R8
U 1 1 5DACBD83
P 2645 2405
F 0 "R8" V 2725 2405 50  0000 C CNN
F 1 "10k" V 2645 2405 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2575 2405 50  0001 C CNN
F 3 "" H 2645 2405 50  0000 C CNN
	1    2645 2405
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR015
U 1 1 5DACBD8D
P 2645 2605
F 0 "#PWR015" H 2645 2605 40  0001 C CNN
F 1 "AGND" H 2645 2535 50  0000 C CNN
F 2 "" H 2645 2605 60  0000 C CNN
F 3 "" H 2645 2605 60  0000 C CNN
	1    2645 2605
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R9
U 1 1 5DACBDB6
P 4535 2855
F 0 "R9" V 4615 2855 50  0000 C CNN
F 1 "47k" V 4535 2855 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4465 2855 50  0001 C CNN
F 3 "" H 4535 2855 50  0000 C CNN
	1    4535 2855
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R3
U 1 1 5DACBDC0
P 3965 1835
F 0 "R3" V 4045 1835 50  0000 C CNN
F 1 "100k" V 3965 1835 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3895 1835 50  0001 C CNN
F 3 "" H 3965 1835 50  0000 C CNN
	1    3965 1835
	0    1    1    0   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:TLV274 U2
U 4 1 5DACBDDE
P 5135 1935
F 0 "U2" H 5185 2135 60  0000 C CNN
F 1 "TL074" H 5285 1735 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 5135 1935 60  0001 C CNN
F 3 "" H 5135 1935 60  0000 C CNN
	4    5135 1935
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R7
U 1 1 5DACBDE8
P 5075 2305
F 0 "R7" V 5155 2305 50  0000 C CNN
F 1 "100k" V 5075 2305 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5005 2305 50  0001 C CNN
F 3 "" H 5075 2305 50  0000 C CNN
	1    5075 2305
	0    1    1    0   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R4
U 1 1 5DACBDF2
P 5975 1935
F 0 "R4" V 6055 1935 50  0000 C CNN
F 1 "1k" V 5975 1935 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5905 1935 50  0001 C CNN
F 3 "" H 5975 1935 50  0000 C CNN
	1    5975 1935
	0    1    1    0   
$EndComp
Wire Wire Line
	3510 2205 3645 2205
Wire Wire Line
	3645 2205 3645 1835
Wire Wire Line
	2645 2605 2645 2555
Wire Wire Line
	3645 1835 3815 1835
Wire Wire Line
	4535 2035 4535 2305
Wire Wire Line
	4535 2035 4635 2035
Wire Wire Line
	5225 2305 5635 2305
Wire Wire Line
	5635 2305 5635 1935
Wire Wire Line
	4925 2305 4535 2305
Connection ~ 4535 2305
Wire Wire Line
	4535 2305 4535 2705
Connection ~ 3645 1835
Connection ~ 5635 1935
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR0106
U 1 1 5DC91035
P 6200 2095
F 0 "#PWR0106" H 6200 2095 40  0001 C CNN
F 1 "AGND" H 6200 2025 50  0000 C CNN
F 2 "" H 6200 2095 60  0000 C CNN
F 3 "" H 6200 2095 60  0000 C CNN
	1    6200 2095
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6200 2095 6200 2035
Wire Wire Line
	6200 2035 6255 2035
$Comp
L power:VCC #PWR0107
U 1 1 5DC8D46C
P 3385 2795
F 0 "#PWR0107" H 3385 2645 50  0001 C CNN
F 1 "VCC" H 3385 2940 30  0000 C CNN
F 2 "" H 3385 2795 50  0001 C CNN
F 3 "" H 3385 2795 50  0001 C CNN
	1    3385 2795
	1    0    0    -1  
$EndComp
Wire Wire Line
	3535 3205 3535 3385
Wire Wire Line
	3385 2795 3385 2855
Wire Wire Line
	3385 3255 3385 3155
$Comp
L KomaFieldKit-PR3-rescue:POT RV-Offset1
U 1 1 5DACBD97
P 3385 3005
F 0 "RV-Offset1" H 3380 2905 50  0000 C CNN
F 1 "100k" H 3385 3005 50  0000 C CNN
F 2 "KOMA:KOMA_RK0903N" H 3385 3005 50  0001 C CNN
F 3 "" H 3385 3005 50  0000 C CNN
	1    3385 3005
	0    1    1    0   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:TLV274 U3
U 2 1 5DCA2DEF
P 3150 5650
F 0 "U3" H 3200 5850 60  0000 C CNN
F 1 "TL074" H 3300 5450 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 3150 5650 60  0001 C CNN
F 3 "" H 3150 5650 60  0000 C CNN
	2    3150 5650
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R13
U 1 1 5DCA2DF9
P 3100 6020
F 0 "R13" V 3180 6020 50  0000 C CNN
F 1 "1k" V 3100 6020 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3030 6020 50  0001 C CNN
F 3 "" H 3100 6020 50  0000 C CNN
	1    3100 6020
	0    1    1    0   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R15
U 1 1 5DCA2E03
P 2650 6220
F 0 "R15" V 2730 6220 50  0000 C CNN
F 1 "100k" V 2650 6220 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2580 6220 50  0001 C CNN
F 3 "" H 2650 6220 50  0000 C CNN
	1    2650 6220
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR0108
U 1 1 5DCA2E0D
P 2650 6420
F 0 "#PWR0108" H 2650 6420 40  0001 C CNN
F 1 "AGND" H 2650 6350 50  0000 C CNN
F 2 "" H 2650 6420 60  0000 C CNN
F 3 "" H 2650 6420 60  0000 C CNN
	1    2650 6420
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R16
U 1 1 5DCA2E17
P 4540 6695
F 0 "R16" V 4620 6695 50  0000 C CNN
F 1 "47k" V 4540 6695 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4470 6695 50  0001 C CNN
F 3 "" H 4540 6695 50  0000 C CNN
	1    4540 6695
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R11
U 1 1 5DCA2E21
P 3970 5650
F 0 "R11" V 4050 5650 50  0000 C CNN
F 1 "100k" V 3970 5650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3900 5650 50  0001 C CNN
F 3 "" H 3970 5650 50  0000 C CNN
	1    3970 5650
	0    1    1    0   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R10
U 1 1 5DCA2E2B
P 4280 5430
F 0 "R10" V 4360 5430 50  0000 C CNN
F 1 "100k" V 4280 5430 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4210 5430 50  0001 C CNN
F 3 "" H 4280 5430 50  0000 C CNN
	1    4280 5430
	-1   0    0    1   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:VV #PWR0109
U 1 1 5DCA2E35
P 4280 5220
F 0 "#PWR0109" H 4280 5320 30  0001 C CNN
F 1 "VV" H 4280 5320 30  0000 C CNN
F 2 "" H 4280 5220 60  0000 C CNN
F 3 "" H 4280 5220 60  0000 C CNN
	1    4280 5220
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:TLV274 U3
U 4 1 5DCA2E3F
P 5140 5750
F 0 "U3" H 5190 5950 60  0000 C CNN
F 1 "TL074" H 5290 5550 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 5140 5750 60  0001 C CNN
F 3 "" H 5140 5750 60  0000 C CNN
	4    5140 5750
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R14
U 1 1 5DCA2E49
P 5080 6120
F 0 "R14" V 5160 6120 50  0000 C CNN
F 1 "100k" V 5080 6120 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5010 6120 50  0001 C CNN
F 3 "" H 5080 6120 50  0000 C CNN
	1    5080 6120
	0    1    1    0   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R12
U 1 1 5DCA2E53
P 5975 5750
F 0 "R12" V 6055 5750 50  0000 C CNN
F 1 "1k" V 5975 5750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5905 5750 50  0001 C CNN
F 3 "" H 5975 5750 50  0000 C CNN
	1    5975 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 6020 2650 6020
Wire Wire Line
	2650 5750 2650 6020
Connection ~ 2650 6020
Wire Wire Line
	2650 6420 2650 6370
Wire Wire Line
	4540 6945 4540 6845
Wire Wire Line
	3650 5650 3820 5650
Wire Wire Line
	4280 5580 4280 5650
Wire Wire Line
	4120 5650 4280 5650
Wire Wire Line
	4280 5220 4280 5280
Connection ~ 4280 5650
Wire Wire Line
	4540 5850 4540 6120
Wire Wire Line
	4540 5850 4640 5850
Wire Wire Line
	5230 6120 5640 6120
Wire Wire Line
	5640 6120 5640 5750
Wire Wire Line
	4930 6120 4540 6120
Connection ~ 4540 6120
Wire Wire Line
	2650 6020 2650 6070
Wire Wire Line
	4280 5650 4640 5650
Wire Wire Line
	4540 6120 4540 6545
Connection ~ 5640 5750
$Comp
L Connector_Generic:Conn_01x02 P7
U 1 1 5DCA2E79
P 2165 5650
F 0 "P7" H 2165 5850 50  0000 C CNN
F 1 "Input B" V 2265 5605 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 2165 5650 50  0001 C CNN
F 3 "" H 2165 5650 50  0000 C CNN
	1    2165 5650
	-1   0    0    1   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR0110
U 1 1 5DCA2E83
P 2420 5710
F 0 "#PWR0110" H 2420 5710 40  0001 C CNN
F 1 "AGND" H 2420 5640 50  0000 C CNN
F 2 "" H 2420 5710 60  0000 C CNN
F 3 "" H 2420 5710 60  0000 C CNN
	1    2420 5710
	1    0    0    -1  
$EndComp
Wire Wire Line
	2420 5710 2420 5650
Wire Wire Line
	2420 5650 2365 5650
Wire Wire Line
	4280 5650 4280 5655
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR0111
U 1 1 5DCA2E9C
P 6205 5910
F 0 "#PWR0111" H 6205 5910 40  0001 C CNN
F 1 "AGND" H 6205 5840 50  0000 C CNN
F 2 "" H 6205 5910 60  0000 C CNN
F 3 "" H 6205 5910 60  0000 C CNN
	1    6205 5910
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6205 5910 6205 5850
Wire Wire Line
	6205 5850 6260 5850
$Comp
L power:VCC #PWR0112
U 1 1 5DCA2EA9
P 3310 6635
F 0 "#PWR0112" H 3310 6485 50  0001 C CNN
F 1 "VCC" H 3310 6780 30  0000 C CNN
F 2 "" H 3310 6635 50  0001 C CNN
F 3 "" H 3310 6635 50  0001 C CNN
	1    3310 6635
	1    0    0    -1  
$EndComp
Wire Wire Line
	3540 6845 3460 6845
Wire Wire Line
	4540 7225 4540 6945
Wire Wire Line
	3540 7225 4540 7225
Wire Wire Line
	3540 7045 3540 7225
Wire Wire Line
	3310 6635 3310 6695
Wire Wire Line
	3310 7095 3310 6995
$Comp
L KomaFieldKit-PR3-rescue:TLV274 U3
U 3 1 5DCA2EB9
P 4040 6945
F 0 "U3" H 4090 7145 60  0000 C CNN
F 1 "TL074" H 4190 6745 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4040 6945 60  0001 C CNN
F 3 "" H 4040 6945 60  0000 C CNN
	3    4040 6945
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR0113
U 1 1 5DCA2EC3
P 3310 7095
F 0 "#PWR0113" H 3310 7095 40  0001 C CNN
F 1 "AGND" H 3310 7025 50  0000 C CNN
F 2 "" H 3310 7095 60  0000 C CNN
F 3 "" H 3310 7095 60  0000 C CNN
	1    3310 7095
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:POT RV-Offset2
U 1 1 5DCA2ECD
P 3310 6845
F 0 "RV-Offset2" H 3305 6745 50  0000 C CNN
F 1 "100k" H 3310 6845 50  0000 C CNN
F 2 "KOMA:KOMA_RK0903N" H 3310 6845 50  0001 C CNN
F 3 "" H 3310 6845 50  0000 C CNN
	1    3310 6845
	0    1    1    0   
$EndComp
Connection ~ 4540 6945
$Comp
L KomaFieldKit-PR3-rescue:TLV274 U3
U 1 1 5DCF33C9
P 8375 5750
F 0 "U3" H 8425 5950 60  0000 C CNN
F 1 "TL074" H 8525 5550 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8375 5750 60  0001 C CNN
F 3 "" H 8375 5750 60  0000 C CNN
	1    8375 5750
	1    0    0    1   
$EndComp
$Comp
L Device:LED_ALT D4
U 1 1 5DCFB4DC
P 8350 5300
F 0 "D4" H 8343 5516 50  0000 C CNN
F 1 "LED_ALT" H 8343 5425 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 8350 5300 50  0001 C CNN
F 3 "~" H 8350 5300 50  0001 C CNN
	1    8350 5300
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R19
U 1 1 5DCFB684
P 7600 5300
F 0 "R19" V 7680 5300 50  0000 C CNN
F 1 "1k" V 7600 5300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7530 5300 50  0001 C CNN
F 3 "" H 7600 5300 50  0000 C CNN
	1    7600 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	7875 5650 7875 5300
Wire Wire Line
	7875 5300 7750 5300
Wire Wire Line
	7875 5300 8200 5300
Connection ~ 7875 5300
Wire Wire Line
	8500 5300 8875 5300
Wire Wire Line
	8875 5300 8875 5750
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR0116
U 1 1 5DD00A2C
P 7400 5350
F 0 "#PWR0116" H 7400 5350 40  0001 C CNN
F 1 "AGND" H 7400 5280 50  0000 C CNN
F 2 "" H 7400 5350 60  0000 C CNN
F 3 "" H 7400 5350 60  0000 C CNN
	1    7400 5350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7400 5350 7400 5300
Wire Wire Line
	7400 5300 7450 5300
Wire Wire Line
	5640 5750 5825 5750
Wire Wire Line
	6125 5750 6260 5750
Text Label 5665 5750 0    39   ~ 8
OutB
Text Label 5660 1935 0    39   ~ 8
OutA
Wire Wire Line
	6125 1935 6255 1935
Wire Wire Line
	5635 1935 5825 1935
Wire Wire Line
	2365 5550 2650 5550
Text Label 7745 5850 2    39   ~ 8
OutB
Wire Wire Line
	7745 5850 7875 5850
$Comp
L KomaFieldKit-PR3-rescue:TLV274 U2
U 1 1 5DD1DC0C
P 8375 1950
F 0 "U2" H 8425 2150 60  0000 C CNN
F 1 "TL074" H 8525 1750 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 8375 1950 60  0001 C CNN
F 3 "" H 8375 1950 60  0000 C CNN
	1    8375 1950
	1    0    0    1   
$EndComp
$Comp
L Device:LED_ALT D3
U 1 1 5DD1DC16
P 8350 1500
F 0 "D3" H 8343 1716 50  0000 C CNN
F 1 "LED_ALT" H 8343 1625 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 8350 1500 50  0001 C CNN
F 3 "~" H 8350 1500 50  0001 C CNN
	1    8350 1500
	1    0    0    -1  
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R18
U 1 1 5DD1DC20
P 7600 1500
F 0 "R18" V 7680 1500 50  0000 C CNN
F 1 "1k" V 7600 1500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7530 1500 50  0001 C CNN
F 3 "" H 7600 1500 50  0000 C CNN
	1    7600 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	7875 1850 7875 1500
Wire Wire Line
	7875 1500 7750 1500
Wire Wire Line
	7875 1500 8200 1500
Connection ~ 7875 1500
Wire Wire Line
	8500 1500 8875 1500
Wire Wire Line
	8875 1500 8875 1950
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR0117
U 1 1 5DD1DC30
P 7400 1550
F 0 "#PWR0117" H 7400 1550 40  0001 C CNN
F 1 "AGND" H 7400 1480 50  0000 C CNN
F 2 "" H 7400 1550 60  0000 C CNN
F 3 "" H 7400 1550 60  0000 C CNN
	1    7400 1550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7400 1550 7400 1500
Wire Wire Line
	7400 1500 7450 1500
Text Label 7745 2050 2    39   ~ 8
OutA
Wire Wire Line
	7745 2050 7875 2050
Wire Wire Line
	3535 3385 4535 3385
$Comp
L KomaFieldKit-PR3-rescue:POT RV-Gain1
U 1 1 5DB83349
P 3360 2355
F 0 "RV-Gain1" H 3355 2255 50  0000 C CNN
F 1 "100k" H 3360 2355 50  0000 C CNN
F 2 "KOMA:KOMA_RK0903N" H 3360 2355 50  0001 C CNN
F 3 "" H 3360 2355 50  0000 C CNN
	1    3360 2355
	1    0    0    -1  
$EndComp
Wire Wire Line
	3210 2205 3210 2355
Wire Wire Line
	3510 2205 3510 2355
Wire Wire Line
	3360 2205 3510 2205
Connection ~ 3510 2205
$Comp
L KomaFieldKit-PR3-rescue:R R6
U 1 1 5DB9D3B0
P 2925 2205
F 0 "R6" V 3005 2205 50  0000 C CNN
F 1 "1k" V 2925 2205 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2855 2205 50  0001 C CNN
F 3 "" H 2925 2205 50  0000 C CNN
	1    2925 2205
	0    1    1    0   
$EndComp
Wire Wire Line
	3210 2205 3075 2205
Wire Wire Line
	2775 2205 2645 2205
Wire Wire Line
	2645 1935 2645 2205
Wire Wire Line
	2645 2205 2645 2255
Connection ~ 2645 2205
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR016
U 1 1 5DACBDA1
P 3385 3255
F 0 "#PWR016" H 3385 3255 40  0001 C CNN
F 1 "AGND" H 3385 3185 50  0000 C CNN
F 2 "" H 3385 3255 60  0000 C CNN
F 3 "" H 3385 3255 60  0000 C CNN
	1    3385 3255
	1    0    0    -1  
$EndComp
Wire Wire Line
	4535 3385 4535 3105
Wire Wire Line
	4535 3105 4535 3005
Connection ~ 4535 3105
$Comp
L KomaFieldKit-PR3-rescue:TLV274 U2
U 3 1 5DACBDAC
P 4035 3105
F 0 "U2" H 4085 3305 60  0000 C CNN
F 1 "TL074" H 4185 2905 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 4035 3105 60  0001 C CNN
F 3 "" H 4035 3105 60  0000 C CNN
	3    4035 3105
	1    0    0    -1  
$EndComp
Wire Wire Line
	4115 1835 4365 1835
$Comp
L KomaFieldKit-PR3-rescue:R R5
U 1 1 5DBA6013
P 4365 1615
F 0 "R5" V 4445 1615 50  0000 C CNN
F 1 "100k" V 4365 1615 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4295 1615 50  0001 C CNN
F 3 "" H 4365 1615 50  0000 C CNN
	1    4365 1615
	-1   0    0    1   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:VV #PWR0118
U 1 1 5DBA601D
P 4365 1405
F 0 "#PWR0118" H 4365 1505 30  0001 C CNN
F 1 "VV" H 4365 1505 30  0000 C CNN
F 2 "" H 4365 1405 60  0000 C CNN
F 3 "" H 4365 1405 60  0000 C CNN
	1    4365 1405
	1    0    0    -1  
$EndComp
Wire Wire Line
	4365 1765 4365 1835
Wire Wire Line
	4365 1405 4365 1465
Connection ~ 4365 1835
Wire Wire Line
	4365 1835 4635 1835
Wire Wire Line
	3685 6020 3820 6020
Wire Wire Line
	3820 6020 3820 5650
$Comp
L KomaFieldKit-PR3-rescue:POT RV-Gain2
U 1 1 5DBABCA6
P 3535 6170
F 0 "RV-Gain2" H 3530 6070 50  0000 C CNN
F 1 "100k" H 3535 6170 50  0000 C CNN
F 2 "KOMA:KOMA_RK0903N" H 3535 6170 50  0001 C CNN
F 3 "" H 3535 6170 50  0000 C CNN
	1    3535 6170
	1    0    0    -1  
$EndComp
Wire Wire Line
	3385 6020 3385 6170
Wire Wire Line
	3685 6020 3685 6170
Wire Wire Line
	3535 6020 3685 6020
Connection ~ 3685 6020
Wire Wire Line
	3385 6020 3250 6020
Connection ~ 3820 5650
Text Notes 2930 1585 2    79   ~ 0
Input 0V - 1,5V
Text Notes 2940 5380 2    79   ~ 0
Input 0V - 2,6V
$Comp
L Connector:AudioJack2_SwitchT J2
U 1 1 5DCB2494
P 6455 1935
F 0 "J2" H 6275 1868 50  0000 R CNN
F 1 "AudioJack2_SwitchT" H 6275 1959 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_QingPu_WQP-PJ398SM_Vertical_CircularHoles" H 6455 1935 50  0001 C CNN
F 3 "~" H 6455 1935 50  0001 C CNN
	1    6455 1935
	-1   0    0    1   
$EndComp
NoConn ~ 6255 1835
$Comp
L Connector:AudioJack2_SwitchT J3
U 1 1 5DCB64B6
P 6460 5750
F 0 "J3" H 6280 5683 50  0000 R CNN
F 1 "AudioJack2_SwitchT" H 6280 5774 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_QingPu_WQP-PJ398SM_Vertical_CircularHoles" H 6460 5750 50  0001 C CNN
F 3 "~" H 6460 5750 50  0001 C CNN
	1    6460 5750
	-1   0    0    1   
$EndComp
NoConn ~ 6260 5650
Wire Wire Line
	2115 1735 2445 1735
Wire Wire Line
	2170 1835 2115 1835
Wire Wire Line
	2170 1895 2170 1835
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR0105
U 1 1 5DC5A126
P 2170 1895
F 0 "#PWR0105" H 2170 1895 40  0001 C CNN
F 1 "AGND" H 2170 1825 50  0000 C CNN
F 2 "" H 2170 1895 60  0000 C CNN
F 3 "" H 2170 1895 60  0000 C CNN
	1    2170 1895
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P5
U 1 1 5DC5A11C
P 1915 1835
F 0 "P5" H 1915 2035 50  0000 C CNN
F 1 "Input A" V 2015 1790 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 1915 1835 50  0001 C CNN
F 3 "" H 1915 1835 50  0000 C CNN
	1    1915 1835
	-1   0    0    1   
$EndComp
$Comp
L KomaFieldKit-PR3-rescue:R R?
U 1 1 5EE3FE38
P 2445 1885
F 0 "R?" V 2525 1885 50  0000 C CNN
F 1 "1M" V 2445 1885 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2375 1885 50  0001 C CNN
F 3 "" H 2445 1885 50  0000 C CNN
	1    2445 1885
	-1   0    0    1   
$EndComp
Connection ~ 2445 1735
Wire Wire Line
	2445 1735 2645 1735
$Comp
L KomaFieldKit-PR3-rescue:AGND #PWR?
U 1 1 5EE41B20
P 2445 2035
F 0 "#PWR?" H 2445 2035 40  0001 C CNN
F 1 "AGND" H 2445 1965 50  0000 C CNN
F 2 "" H 2445 2035 60  0000 C CNN
F 3 "" H 2445 2035 60  0000 C CNN
	1    2445 2035
	1    0    0    -1  
$EndComp
$EndSCHEMATC
