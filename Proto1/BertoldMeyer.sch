EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "Bertold Meyer Analog Interface"
Date "2019-10-15"
Rev "0"
Comp "KOMA Elektronik"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1525 3150 2825 1875
U 5DAA151B
F0 "Power / Mechanic" 60
F1 "PowerMechanic.sch" 60
$EndSheet
$Sheet
S 6575 3150 2825 1875
U 5DAA23CB
F0 "Analog Interface" 60
F1 "Analog Interface.sch" 60
$EndSheet
Text Notes 3850 1600 0    157  ~ 31
Bertolt Meyer Analog Interface
Text Notes 6550 5850 0    118  ~ 0
To Do:\n- add 1M resistors to ground on the opamp inputs\n- separate grounds (optocouplers)
Text Notes 1800 4175 0    157  ~ 31
Power / Mechanic
Text Notes 6950 4175 0    157  ~ 31
Analog Interfaces
Text Notes 1500 6050 0    118  ~ 0
Idears:\n- Switch between several channels (touch plates)\n- Gyros\n- Triggers / Gates
$EndSCHEMATC
